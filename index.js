/**
 * Get a schema by its id
 * @param {string} id 
 */
function get(id) {
    id = id.replace('http://schemas.infopack.io/', '');
    return require('./schemas/' + id);
}

module.exports.get = get;
